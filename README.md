# deepgreen API documentation

Welcome to deepgreen APIs documentation! We are excited to see what you will do with our APIs, and to get started quickly you will find below some instructions to make your first API calls.

This documentation is a work in progress and will be updated regularly. We will happily listen to your feedback to improve both the APIs and documentation. Reach out to us [via email](contact@deepgreen.ai).

## Authentication

The API currently only supports http basic (protected via ssl). Make sure to check your mailbox for your deepgreen credentials.
We will be working on additional authentication methods in the coming weeks. For more details contact us [via email](contact@deepgreen.ai).

## Rate Limiting

We are still small, and we want to scale our infrastructure with your usage. We are introducing rate limiting to keep things stable, but we're happy to provide larger quotas quickly depending on your needs.

* API calls per hour: 30
* API calls per minute: 5
* API calls per second: 1

## API Usage

The following code snippets upload a picture to the API over HTTP and receives a json response with deepgreen's analytics.

```bash
# Change "username" and "password" with your deepgreen API credentials, for more details contact us at contact@deepgreen.ai
declare USERNAME=username
declare PASSWORD=password
wget https://storage.googleapis.com/deepgreen_public/cannabis_light_mildew_raw.jpg
curl -X POST "https://api.deepgreen.ai/v1/images" -u ${USERNAME}:${PASSWORD} -H "Content-Type: application/octet-stream" --data-binary "@cannabis_light_mildew_raw.jpg"
```

## Java

Coming soon

## Python
Coming soon

## react component

We provide a react Component to easily display analytics over an image.

Your deepgreen credentials should be stored securely and should never leak to the frontend.

Coming soon.

## Other languages

We are happy to help! Let us know how, and we will happily provide you with more details on how to use our APIs from any platform.

## API Response

After ~45 seconds (we will bring this down to ~5 seconds by the end of the year) you should receive a json response including all deepgreen analytics. An empty array is returned when no anomaly is found in the picture.

## Response

```json
[
  {
    "prediction":"powdery_mildew",
    "confidence":0.9998237490653992,
    "x1":0.8155579566955566,
    "y1":0.31962496042251587,
    "x2":0.8963850140571594,
    "y2":0.3906838595867157
  },
  {
    "prediction":"powdery_mildew",
    "confidence":0.9997884631156921,
    "x1":0.46163392066955566,
    "y1":0.6123434901237488,
    "x2":0.5332764983177185,
    "y2":0.6914430856704712
  },
  {
    "prediction":"powdery_mildew",
    "confidence":0.9997135996818542,
    "x1":0.09132793545722961,
    "y1":0.3250843584537506,
    "x2":0.17237505316734314,
    "y2":0.4227452874183655
  },
  {
    "prediction":"powdery_mildew",
    "confidence":0.9988330006599426,
    "x1":0.4828929305076599,
    "y1":0.2941349446773529,
    "x2":0.5537889003753662,
    "y2":0.34284666180610657
  }
]
```

## Error codes

HTTP Status

* 401: Authentication failure
* 429: Too many requests